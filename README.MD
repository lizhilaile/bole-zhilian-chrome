[github](https://github.com/dasuiyuanhao/bole-zhilian-chrome)
[gitee](https://gitee.com/lizhilaile/bole-zhilian-chrome)
## 1.介绍
<h3>声明：仅供学习参考，如果出现经济损失概不负责，请勿用于商业用途！</h3>
<br/>
<font color="orange"><h3>您的Star是我继续前进的动力，如果喜欢请右上角帮忙点个Star</h3></font>
<br/>
<h3>提醒：本项目不再维护，敬请谅解！也不提供其他版本和教学，谨防上当受骗。</h3>
<br/>

zhaopin-boss-chrome，伯乐2号，招聘自动化工具，是Chrome扩展程序，省时间90%。模拟人的操作，自动筛选，自动打招呼，自动沟通。智联招聘插件，智联插件，[开源代码地址](https://gitee.com/lizhilaile/bole-zhilian-chrome)。

只能在[智联招聘](https://rd6.zhaopin.com/)网站使用,为您提供高效的自动化辅助工具

另外，BOSS直聘自动化工具：[开源代码地址zhaopin-boss-chrome](https://gitee.com/lizhilaile/zhaopin-boss-chrome)

### 技术亮点：
#### 1、使用Input.dispatchMouseEvent解决isTrusted为true的判断，触发按钮点击事件。
#### 2、解决iframe中元素的定位问题。
#### 3、实现Chrome扩展程序存储数据，例如保存和读取配置项。
#### 4、页面元素筛选，按照既定逻辑自动操作；对于自定义封装输入框，能够进行模拟输入和触发事件。


## 2.版本说明

### V2.0.0
【推荐人才】筛选条件增加年龄范围、性别、求职期望地点、牛人活跃状态。

增加重置筛选条件配置功能。增加自动打招呼功能。


【互动】对于已读的人员进行筛选，标记颜色；自动进行发消息；使用快捷键滚动到下一个、上一个。
    
## 3.使用说明

### 3.1 安装
  您可以下载源代码，在chrome://extensions 下打开开发者模式，加载已解压的扩展程序。

  [Chrome安装扩展程序教程](https://jingyan.baidu.com/article/148a19216b72900c70c3b176.html)
### 3.2 使用
  1.打开【推荐人才】页面，左下角会显示控制台(点击页面右上角隐藏按钮可以隐藏)，配置筛选条件。

  2.滚动到页面底部加载简历，也可以一键加载全部，点击筛选全部。
  
  满足筛选条件的简历：未沟通的标记橙色，已沟通的标记淡蓝色。已过滤专科学历。

  点击【打招呼】按钮，勾选了“打招呼后自动滚动到下一个”，会自动滚动到下一个满足条件的简历。

  开始体验丝滑般的沟通吧！ 

  
## 联系方式
### 作者：dasuiyuanhao

### 电子邮箱：dasuiyuanhao@sina.com

### 微信号：dasuiyuanhao
<div  align="center"> 
<img src="https://gitee.com/lizhilaile/blog/raw/master/images/weixin_dasuiyuanhao_800.jpg" width = "400" alt="微信二维码：dasuiyuanhao" align="center" />
</div>
</br>

## 如果您使用后节省了时间，请打赏￥31.4元(一顿外卖)，帮捧捧场，请右上角点个Star，谢谢！
<div  align="center"> 
<img src="https://gitee.com/lizhilaile/blog/raw/master/images/wodeweixinshoukuanma.png" width = "400" height = "400" alt="微信支付，微信号：dasuiyuanhao" align="center" />
</div>
</br>
<div  align="center"> 
<img src="https://gitee.com/lizhilaile/blog/raw/master/images/wodezhifubaoshoukuanma.png" width = "400" height = "400" alt="支付宝支付" align="center" />
</div>

### 声明：仅供学习参考，如果出现经济损失概不负责，请勿用于商业行为！
